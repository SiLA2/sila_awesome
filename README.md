# [Awesome SiLA](https://gitlab.com/SiLA2/sila_awesome)

> A curated list of awesome SiLA 2, implementations, applications, drivers, clients, devices, servers that use the SiLA 2 standard.

## Contents  

- [Awesome SiLA](#)
  - [Contents](#contents)
  - [Introduction to SiLA 2](#introduction-to-sila-2)
    - [What is SiLA 2](#what-is-sila-2)
    - [Who Is This For](#who-is-this-for)
    - [Understanding SiLA 2 Components](#understanding-sila-2-components)
      - [What is a Client](#what-is-a-client)
      - [What is a Server](#what-is-a-server)
    - [Terminology](#terminology)
  - [Navigating SiLA 2 Resources](#navigating-sila-2-resources)
  - [Implementations](#implementations)
    - [Standard Implementations](#standard-implementations)
    - [Clients](#clients)
    - [Servers](#servers)
    - [Other](#other)
  - [Resources](#resources)
    - [Standard](#standard)
    - [Documentation](#documentation)
    - [Examples](#examples)
    - [Tutorials](#tutorials)
  - [Contributing](#contributing)

---

## Introduction to SiLA 2

### What is SiLA 2

[SiLA](https://sila-standard.com/) (Standard in Laboratory Automation) is a global initiative aimed at standardizing connectivity and data exchange in life science lab automation. It focuses on facilitating orchestration and ensuring FAIR (Findable, Accessible, Interoperable, Reusable) data principles, thereby enhancing the user experience in laboratory software and instrumentation.
This initiative is backed by leading life science companies, device manufacturers, and software suppliers worldwide.

### Who Is This For

This curated list and the resources provided are designed for a wide range of users:

- For software developers and engineers who are familiar with coding and SiLA concepts, you'll find direct links to various implementations and technical resources.
- For scientists and laboratory personnel who are newer to software and SiLA, this document aims to provide a clear and accessible entry point into the world of laboratory automation.

## Understanding SiLA 2 Components

### What is a Client

Clients in the SiLA 2 ecosystem are tools used to interact with SiLA servers. Their primary functions include accessing data or controlling laboratory instrumentation. Many lab orchestration software suites incorporate SiLA clients, and continuous development is expanding their availability and capabilities.

### What is a Server

Colloquially referred as "drivers", SiLA Servers are the backbone of the SiLA 2 framework. They facilitate communication between laboratory instruments and client software. Each server-client interaction is designed in accordance with the SiLA standards, ensuring consistency and reliability in lab automation.

### Terminology
In line with the SiLA 2 standard terminology, the terms "SiLA Server," "SiLA Device," and "SiLA Client" are used to describe the respective components. These terms focus on what the component is, rather than how it is implemented (e.g., as a driver over an existing interface). While "Driver" can be used, our ultimate goal at SiLA is to motivate companies, both software and hardware vendors, to integrate native SiLA support into their products.

## Navigating SiLA 2 Resources

Accessing and utilizing the most of the resources available in this document will be done through websites such as GitLab/GitHub or vendor websites.

- **GitLab / GitHub Repositories:** The provided GitLab / GitHub links lead to repositories containing code and documentation. For those unfamiliar with Git or GitLab / GitHub, we recommend exploring introductory guides to them, which will assist in understanding how to download repositories, navigate code, and contribute.
- **Installation and Setup:** The complexity of setting up and installing these tools can vary. However an upcoming user friendly SiLA Installer project is being developed to ease the installation process for any kind of users.

We recognize the diversity in the technical background of our community and strive to make SiLA 2 as inclusive and accessible as possible. Your feedback and contributions help us bridge any gaps and improve user experience.

## Implementations

### Standard Implementations
- [SiLA Java](https://gitlab.com/SiLA2/sila_java) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_java) - Official Java reference implementation of the SiLA2 Standard.
- [SiLA Python](https://gitlab.com/SiLA2/sila_python) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_python) - Official Python reference implementation of the SiLA2 Standard.
- [SiLA C#](https://gitlab.com/SiLA2/sila_csharp) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_csharp) - Official C# reference implementation of the SiLA2 Standard.
- [SiLA2 Grpc Dotnet](https://gitlab.com/Chamundi/SiLA2-grpc-dotnet) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/Chamundi/SiLA2-grpc-dotnet) - Platform-independent SiLA2 .NET 6 (LTS) implementation with recommended https://github.com/grpc/grpc-dotnet.
- [SiLA Tecan](https://gitlab.com/SiLA2/vendors/sila_tecan) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/vendors/sila_tecan) - Alternative implementation of the SiLA 2 standard using .NET.
- [SiLA C++](https://gitlab.com/SiLA2/sila_cpp) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_cpp) - Official C++ reference implementation of the SiLA2 Standard.
- [SiLA Javascript](https://gitlab.com/SiLA2/sila_js) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_js) - Official Javascript reference implementation of the SiLA2 Standard.
- [SiLA Haskell](https://gitlab.com/SiLA2/sila_haskell) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_haskell) - Official Haskell reference implementation of the SiLA2 Standard.
- [SiLA UniteLabs](https://gitlab.com/unitelabs/connector-framework) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/unitelabs/connector-framework) - Python implementation including cloud-connectivity by UniteLabs (SiLA 2 1.1).


### Clients
- [Universal SiLA Client](https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client) - Feature full client that enables scientists and non technical users to control any SiLA2 Servers in a user friendly manner through their Phone, Tablet and PC.
- [SiLA Orchestrator](https://github.com/FlorianBauer/sila-orchestrator) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/FlorianBauer/sila-orchestrator) - A simple, dynamic SiLA 2 compliant client for coordinating various services.
- [SiLA 2 Manager](https://gitlab.com/lukas.bromig/sila2_manager) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/lukas.bromig/sila2_manager) - Framework for laboratory automation with a modern IoT interface.
- [SiLA Box](https://gitlab.com/SiLA2/sila_box) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_box) - Manage SiLA server/client and the network configurations using a Raspberry Pi.
- [SiLA Edge Gateway](https://gitlab.com/SiLA2/sila_edge_gateway) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_edge_gateway) - The SiLA Edge Gateway is a middleware between SiLA Servers in the laboratory and SiLA Clients in the cloud.
- [SiLA Camunda](https://gitlab.com/SiLA2/vendors/sila_camunda) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/vendors/sila_camunda) - Implementation allowing Camunda BPM to discover, connect to, and call SiLA Servers using the Camunda BPM Engine.
- [SiLA Browser](https://gitlab.com/unitelabs/integrations/sila2/sila-browser) - The new SiLA 2 Browser from UniteLabs. It allows users to discover and operate any SiLA 2 server in the same network from your web browser. A lightweight, easy to use application.
- [niceLAB - SiLA version coexistence scheduling software](https://www.equicon.de/en/laboratory-automation/scheduling-software-nicelab) - A dynamic scheduling software used for controlling automated laboratory systems.
- [Workflow Automation Flowable & Camunda](https://roche.com) - In this POC it was shown how automation software can be developed with free and open source components and together with the community.
- [UniteLabs Edge Gateway](https://unitelabs.ch/contact) - An Edge Gateway that takes every SiLA 2 Server to the cloud and makes them SiLA 2 1.1 compliant. Based on the UniteLabs SiLA 2 TypeScript implementation.


### Servers
- [SiLA ROS](https://gitlab.com/SiLA2/sila_ros) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/SiLA2/sila_ros) - Enables interaction with the Robot Operating System (ROS). On the ROS side, an action server is implemented in python, which supports Sila.
- [Franka Emika Panda robot arm Controller](https://github.com/FlorianBauer/panda-controller) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/FlorianBauer/panda-controller) - A SiLA 2 compliant controller for an Franka Emika Panda robot arm.
- [Opentrons OT-2 liquid handling robot controller](https://github.com/FlorianBauer/ot2-controller) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/FlorianBauer/ot2-controller) - A SiLA 2 compliant controller for an Opentrons OT-2 liquid handling robot.
- [Andrew Alliance SiLA](https://gitlab.com/Timort/andrew-alliance-sila) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/Timort/andrew-alliance-sila) - A SiLA compliant server that allows to run protocols on OneLab using a controlled web browser.
- [CETONI SiLA 2 SDK](https://github.com/CETONI-Software/sila_cetoni) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni) - Open Source part of the SiLA CETONI SDK including the SiLA servers for CETONI devices.
- [CETONI SiLA 2 Core](https://github.com/CETONI-Software/sila_cetoni_core) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni_core) - SiLA CETONI SDK package including all core features.
- [CETONI Nemesys High Precision Syringe Pumps Driver](https://github.com/CETONI-Software/sila_cetoni_pumps) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni_pumps) - SiLA CETONI SDK package including all pump features for CETONI Nemesys syringe pumps.
- [CETONI I/O Modules Driver](https://github.com/CETONI-Software/sila_cetoni_io) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni_io) - SiLA CETONI SDK package including all I/O features for CETONI I/O modules.
- [CETONI Reaction Modules Driver](https://github.com/CETONI-Software/sila_cetoni_controllers) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni_controllers) - SiLA CETONI SDK package including all controller features for CETONI Reaction modules.
- [CETONI Sample Handlers Driver](https://github.com/CETONI-Software/sila_cetoni_motioncontrol) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni_motioncontrol) - SiLA CETONI SDK package including all motioncontrol features for CETONI Sample Handlers.
- [CETONI Valves Driver](https://github.com/CETONI-Software/sila_cetoni_valves) [![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/CETONI-Software/sila_cetoni_valves) - SiLA CETONI SDK package including all valve features for CETONI Valve modules.
- [Basic MassSpec Interface](https://novartis.com) - Port an existing in-house developed generic interface to mass spectrometers to the SiLA2 standard.
- [ThermoFisher RapidStak Driver](https://tu-berlin.de) - A SiLA2 driver for the TF RapidStak based on the Storage Feature, implemented in Python. Device is connected to a RaspberryPi and is providing the SiLA Server via WiFi.
- [PIXL Colony Picker](https://singerinstruments.com) - TODO add description.
- [LC-MS Plate Feeder System](https://idorsia.com) - The purpose of this system in our chemical QC group is to serve 384-well plates placed into any of the input shelves, to one of two LC-MS Systems (Waters).
- [Bioshake QX Driver](https://gitlab.com/sila-driver-group/bioshake-qx) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/sila-driver-group/bioshake-qx) - SiLA2 driver for controlling a BioShake QX plate shaker from QInstruments.
- [Teleshake 1536 Driver](https://gitlab.com/sila-driver-group/teleshake) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/sila-driver-group/teleshake) - SiLA2 driver for controlling a Teleshake 1536 plate shaker from ThermoScientific.
- [Universal Robots (URX) Driver](https://gitlab.com/sila-driver-group/ur-robot) [![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/sila-driver-group/ur-robot) - SiLA2 driver for controlling a Universal Robots robot arm.

### Other
- [UniteLabs Connectivity Platform](https://unitelabs.ch) - A cloud application leveraging the SiLA 2 1.1 cloud-connectivity. UniteLabs is a connectivity platform for biotech companies. UniteLabs Connect is an integration layer that connects laboratory hard- & software systems across vendor barriers and enables seamless integration within minutes. With UniteLabs Automate, users can rapidly build and deploy custom workflows to automate processes or supercharge existing lab software. It also contains a Python client library for API access.
- [Workflow Validation](https://blueprintgenetics.com/) - This solution improves the security in handling patient DNA extracted from blood samples in a next generation sequencing workflow.
- [Mobile sealing station implemented with niceLAB](https://roche.com) - The control system of a mobile sealing station at Roche in Basel, which could be used by several labs, has been realized by using the scheduling software niceLAB from EQUIcon.
- [SiLA2 Add-On for CETONI Elements](https://cetoni.com/cetoni-elements) - A laboratory automation software with a powerful scripting system for device and process control.

## Resources

### Standard
- [SiLA Base](https://gitlab.com/SiLA2/sila_base) ![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white) - SiLA 2 Feature Definitions, framework protos and Schema.
- [SiLA 2 Part (A)](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM) - Overview, Concepts and Core Specification.
- [SiLA 2 Part (B)](https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA) - Mapping Specification.
- [SiLA 2 Part (C)](https://docs.google.com/document/d/1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY) - Standard Features Index.


### Documentation
- [SiLA Website](https://sila-standard.com/) - Official SiLA Website.
- [SiLA Wiki](https://sila2.gitlab.io/sila_base/) - SiLA Wiki.


### Examples
- [SiLA Interoperability](https://gitlab.com/SiLA2/sila_interoperability) ![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white) - This repository shall host the interoperability tests between the different implementation languages.
- [SiLA Nespresso](https://github.com/QTimort/SiLA-Nespresso) ![Github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white) - A fun implementation of a SiLA compliant server that allows clients to see available Nespresso (CH) coffees and to make a shopping list.


### Tutorials
- [SiLA Training](https://gitlab.com/SiLA2/sila_training) ![Gitlab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white) - Repository with example implementations to be used for SiLA Training.

## Contributing
Contributions in the form of "feature" requests on the [Official SiLA Awesome GitLab Repository](https://gitlab.com/SiLA2/sila_awesome), merge requests, and general feedback are highly encouraged. The SiLA 2 Core working group will review and process your merge requests diligently. How to [edit a file online on GitLab](https://docs.gitlab.com/ee/user/project/repository/web_editor.html) and how to [create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).
